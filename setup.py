# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

try:
    long_description = open("README.rst").read()
except IOError:
    long_description = ""

setup(
    name="aioterm",
    version="0.1.0",
    description="Terminal in browser using ptyprocess + term.js for aiohttp+websocket",
    license="MIT",
    author="Michael Murtaugh",
    packages=find_packages(),
    install_requires=['ptyprocess', 'aiohttp'],
    long_description=long_description,
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7",
    ]
)
