import os
import logging
from aiohttp import web
from .websocket import TermSocket


class TermHandler():
    def __init__(self, term_manager, index=None, termjs=None):
        self.term_manager = term_manager
        self._logger = logging.getLogger(__name__)
        if index is not None:
            self.index_path = index
        else:
            self.index_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", "index.html")
        if termjs is not None:
            self.termjs_path = termjs
        else:
            self.termjs_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", "term.js")

    async def handler(self, request):
        # following https://github.com/aio-libs/aiohttp-demos/blob/master/demos/chat/aiohttpdemo_chat/views.py
        # alt you can also catch web.HTTPException on prepare
        ws = web.WebSocketResponse()
        ws_ready = ws.can_prepare(request)
        if not ws_ready.ok:
            # allow it to also serve the term.js
            name = request.match_info.get("name", "")
            self._logger.info(f"TermSock.http request {name}")
            if name == "":
                return web.FileResponse(self.index_path)
            elif name == "term.js":
                return web.FileResponse(self.termjs_path)
            else:
                raise web.HTTPNotFound()
            # return aiohttp_jinja2.render_template('index.html', request, {})
        ts = TermSocket(self.term_manager)
        return await ts.handle(request, ws)
