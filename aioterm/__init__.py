"""
Terminals served to xterm.js using aiohttp websockets
Adapted from Terminado
"""

from .handler import TermHandler
from .websocket import TermSocket
from .managers import (TermManagerBase, SingleTermManager, UniqueTermManager)
import logging

__version__ = '0.1.0'




